package com.crosses.app;

import java.util.Scanner;

public class Menu {
    private static final Menu INSTANCE = new Menu();

    private Menu() {}

    public static Menu getInstance() {
        return INSTANCE;
    }

    public int drawMainPage(GameType[] gameTypes, int exitGameCode) {
        int selection;
        Scanner input = new Scanner(System.in);

        System.out.println("Choose from these choices");
        System.out.println("-------------------------\n");

        for (GameType gameType : gameTypes) {
            System.out.println(gameType.getCode() + " - " + gameType.getDisplayName());
        }

        System.out.println(exitGameCode + " - Quit");

        selection = input.nextInt();

        return selection;
    }
}
