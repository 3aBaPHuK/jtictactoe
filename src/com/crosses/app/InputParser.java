package com.crosses.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputParser {
    public static final InputParser INSTANCE = new InputParser();

    public int[] getIntCoords() {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input;

        try {
            input = reader.readLine();

            int[] coords = stringToIntArray(input.split("\\s"));

            if (coords.length != 2) {
                System.out.println("Only 2 digits may be used");
            }

            return coords;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private int[] stringToIntArray(String[] array) {
        int[] intArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            String cord = array[i];
            intArray[i] = Integer.parseInt(cord);
        }

        return intArray;
    }

    private InputParser() {}
}
