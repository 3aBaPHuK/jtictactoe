package com.crosses.app;

public class Field {
    private final int fieldSize;

    public static final char DEFAULT_PLACEHOLDER = '-';

    public static final char DEFAULT_CROSS_SYMBOL = 'X';

    public static final char DEFAULT_ZERO_SYMBOL = 'O';

    private static final int DEFAULT_SIZE = 3;

    private final char[][] items;

    public Field(int size) {
        fieldSize = size;
        items = new char[size][size];
        setDefaultValues();
    }

    public Field() {
        this(DEFAULT_SIZE);
    }

    public boolean drawSymbol(int x, int y, char symbol) {
        int[] coordinates = new int[2];

        coordinates[0] = x - 1;
        coordinates[1] = y - 1;

        if (!checkSymbolAlreadySet(coordinates)) {
            items[x - 1][y - 1] = symbol;

            return true;
        }

        System.out.println("In that coordinates symbol already set");

        return false;
    }

    public char checkWinner() {
        boolean crossIsWinner = isSymbolWin(DEFAULT_CROSS_SYMBOL);
        boolean zeroIsWinner = isSymbolWin(DEFAULT_ZERO_SYMBOL);

        return crossIsWinner ? DEFAULT_CROSS_SYMBOL : (zeroIsWinner ? DEFAULT_ZERO_SYMBOL : DEFAULT_PLACEHOLDER);
    }

    private boolean isSymbolWin(char symbol) {
        boolean isSymbolWin;
        int x = 0;

        do {
            isSymbolWin = checkColumn(items[x], symbol);
            x++;
        } while (!isSymbolWin && x < fieldSize);

        if (isSymbolWin) {

            return true;
        }


        int j = 0;

        do {
            isSymbolWin = checkLine(j, symbol);
            j++;
        } while (!isSymbolWin && j < fieldSize);

        if (isSymbolWin) {

            return true;
        }

        isSymbolWin = checkDiagonal(symbol);

        return isSymbolWin;
    }

    private boolean checkDiagonal(char symbol) {
        int rightDiagonalMatchedCount = 0;

        for (int x = 0; x < fieldSize; x++) {
            if (x > fieldSize - 1) {

                break;
            }

            if (items[x][x] == symbol) {
                rightDiagonalMatchedCount++;
            }
        }

        int leftDiagonalMatchedCount = 0;

        for (int x = fieldSize - 1; x >= 0; x--) {
            int y = (fieldSize - 1) - x;

            if (items[x][y] == symbol) {
                leftDiagonalMatchedCount++;
            }
        }

        return leftDiagonalMatchedCount == fieldSize || rightDiagonalMatchedCount == fieldSize;
    }

    private boolean checkLine(int i, char symbol) {
        int matchedCount = 0;

        for (int j = 0; j < fieldSize; j++) {
            if (items[j][i] == symbol) {

                matchedCount++;
            }
        }

        return matchedCount == fieldSize;
    }

    private boolean checkColumn(char[] lineItems, char symbol) {
        int matchedItemsCount = 0;

        for (char lineItem : lineItems) {
            if (lineItem == symbol) {
                matchedItemsCount++;
            }
        }

        return lineItems.length == matchedItemsCount;
    }

    public void setDefaultValues() {
        for (int i = 0; i < fieldSize; i++) {
            setDefaultLine(i);
        }
    }

    private boolean checkSymbolAlreadySet(int[] coordinates) {
        return items[coordinates[0]][coordinates[1]] != DEFAULT_PLACEHOLDER;
    }

    private void setDefaultLine(int lineNum) {
        for (int i = 0; i < fieldSize; i++) {
            items[i][lineNum] = DEFAULT_PLACEHOLDER;
        }
    }

    public void drawField() {
        for (int i = 0; i < fieldSize; i++) {
            drawLine(i);
            System.out.println();
        }
    }

    private void drawLine(int lineNum) {
        for (int i = 0; i < fieldSize; i++) {
            System.out.print("[" + items[i][lineNum] + "]");
        }
    }
}
