package com.crosses.app;

public enum GameType {
    HOT_CHAIR_TYPE(1, "Play on hot chair"), NETWORK_TYPE(2, "Play by network"), AI_TYPE(3, "Play with AI");

    private final int code;

    private final String displayName;

    GameType(int code, String displayName) {
        this.code = code;
        this.displayName = displayName;
    }

    public int getCode() {
        return code;
    }

    public String getDisplayName() {
        return displayName;
    }
}
