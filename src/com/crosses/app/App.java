package com.crosses.app;

public class App {

    private char latestStepSymbol = Field.DEFAULT_CROSS_SYMBOL;

    private static final int EXIT_GAME_CHOICE = 4;

    private boolean isStarted;

    public App() {
        isStarted = true;

        int userChoice = Menu.getInstance().drawMainPage(GameType.values(), EXIT_GAME_CHOICE);

        if (userChoice == EXIT_GAME_CHOICE) {

            return;
        }

        for (GameType gameType : GameType.values()) {
            if (gameType.getCode() == userChoice) {
                run(gameType);
            }
        }
    }

    private void run(GameType gameType) {
        Field field = new Field(5);
        field.drawField();

        while (isStarted) {
            InputParser inputParser = InputParser.INSTANCE;

            makeStep(inputParser.getIntCoords(), field);

            char winner = field.checkWinner();

            if (winner != Field.DEFAULT_PLACEHOLDER) {
                System.out.println("WINNER IS [" + winner + "]");

                isStarted = false;

                continue;
            }

            System.out.println("No winner yet");
        }
    }

    private void makeStep(int[] coords, Field field) {
        if (field.drawSymbol(coords[0], coords[1], latestStepSymbol)) {

            latestStepSymbol = latestStepSymbol == Field.DEFAULT_CROSS_SYMBOL ? Field.DEFAULT_ZERO_SYMBOL : Field.DEFAULT_CROSS_SYMBOL;
        }

        field.drawField();
    }
}
